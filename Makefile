-include Makefile.overrides

IMAGE ?= kiada:0.1
FULL_IMAGE_TAG = "$(PREFIX)$(IMAGE)"

all: image push

image:
	@echo "Building image $(FULL_IMAGE_TAG)"
	docker build -t $(FULL_IMAGE_TAG) ./

push:
	docker push $(FULL_IMAGE_TAG)

luksa-run:
	PREFIX=nexus.infra.local:8082/mg/ make build push && kubectl delete po kiada --force --grace-period 0 ; kubectl apply -f ./k8s/kiada.pod.yaml && kubectl wait --for=condition=Ready pod/kiada && kubectl port-forward kiada 8080